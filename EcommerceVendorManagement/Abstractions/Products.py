from abc import ABC, abstractmethod


class product(ABC):

    @abstractmethod
    def add_product(self,prodKey, prodVal):
        pass

    @abstractmethod
    def search_product(self,prodKey):
        pass

    @abstractmethod
    def all_products(self):
        pass
