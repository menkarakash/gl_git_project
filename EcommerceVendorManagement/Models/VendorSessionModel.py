

class vendorSessionModel:

    def __init__(self):
        self.__vendorSessionInfo = {}

    def isUserLoggedIn(self,userName):
        for key in self.getVendorSessionInfo.keys():
            if key == userName:
                return True
        return False

    def setVendorSessionInfo(self, userName):
        self.__vendorSessionInfo[userName] = "Active"
        print(f" User {userName} Session is Active ")

    @property
    def getVendorSessionInfo(self):
        return self.__vendorSessionInfo

    def getVendorSessionInfoForUser(self,userName):
        return self.__vendorSessionInfo.get(userName)

    def removeUserFromVendorSessionInfo(self, userName):
        self.__vendorSessionInfo.pop(userName)
        print(f" User {userName} Session is InActive ")

