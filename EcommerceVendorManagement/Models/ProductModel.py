class productModel:

    def __init__(self):
        self.__prod = {}

    def getProd(self):
        return self.__prod

    def search_product(self, prodKey):
        if None==self.__prod.get(prodKey):
            print(f" Enter Product Name for search {prodKey} is not present in system ")
        else:
            return self.__prod.get(prodKey)

    def removeProduct(self, prodKey):
        self.__prod.pop(prodKey)
        return print(f" Product {prodKey} is Removed ")

    def add_product(self, prodKey, prodVal, prodType, prodAvailableQuantity):
        self.__prod[prodKey] = {
            "product_name": prodKey,
            "product_type": prodType,
            "available_quantity": prodAvailableQuantity,
            "product_price": prodVal
        }
        print(f" Product {prodKey} is Added ")

    def get_all_products(self):
        if len(self.__prod)==0:
            return print(" There is no Product in system ")
        else:
            print(self.__prod.keys())
            return self.__prod.keys()
