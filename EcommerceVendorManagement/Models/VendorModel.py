class vendorModel:

    def __init__(self):
        self.__userInfo = {}

    def setUserInfo(self, userName, Password):
        self.__userInfo[userName] = Password
        print(f"User {userName} Added in System successfully!")

    def isUserLoggedIn(self,userName):
        for key in self.getUserInfo.keys():
            if key == userName:
                return True
        return False

    @property
    def getUserInfo(self):
        return self.__userInfo

    def logout(self,userName):
        self.__userInfo.pop(userName)
        print(f"User {userName} logged out successfully!")

    def login(self,userName,Password):
        if self.__userInfo.get(userName)==Password:
            #print(f"User {userName} logged in successfully!")
            return True
        else:
            #print(" Invalid username or password. ")
            return False

