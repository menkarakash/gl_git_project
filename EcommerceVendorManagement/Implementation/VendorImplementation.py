from Ecommerce_Vendor_Management.Abstractions.Vendor import vendor
from Ecommerce_Vendor_Management.Models.VendorModel import vendorModel
from Ecommerce_Vendor_Management.Models.VendorSessionModel import vendorSessionModel


class vendorImpl(vendor):
    def __init__(self):
        self.vendorSessionModelObj = vendorSessionModel()
        self.vendorModelObj = vendorModel()

    def isUserPresentAndActiveInSystem(self, userName):
        if self.vendorModelObj.isUserLoggedIn(userName) and self.vendorSessionModelObj.isUserLoggedIn(userName):
            return True
        else:
            False

    def setUserInfo(self, userName, Password):
        self.vendorModelObj.setUserInfo(userName, Password)

    def login(self, userName, Password):
        if self.vendorModelObj.login(userName, Password):
            print(f"User {userName} logged in successfully!")
            self.vendorSessionModelObj.setVendorSessionInfo(userName)
            return True
        else:
            print(" Invalid username or password. ")
            return False

    def logout(self,userName):
        self.vendorModelObj.logout(userName)
        self.vendorSessionModelObj.removeUserFromVendorSessionInfo(userName)
