from Ecommerce_Vendor_Management.Abstractions.Products import product
from Ecommerce_Vendor_Management.Models.ProductModel import productModel
from Ecommerce_Vendor_Management.Models.VendorSessionModel import vendorSessionModel


class productImpl(product):

    def __init__(self):
        self.prodModelObj = productModel()
        self.vendorSessionModelObj = vendorSessionModel()
        self.user=""

    def setUser(self,user):
        self.user=user

    def search_product(self, prodKey):
        return self.prodModelObj.search_product(prodKey)

    def add_product(self, prodKey, prodVal, prodType, prodAvailableQuantity):
        if "Active".__eq__(self.vendorSessionModelObj.getVendorSessionInfoForUser(self.user)):
            self.prodModelObj.add_product(prodKey, prodVal, prodType, prodAvailableQuantity)

    def all_products(self):
        return self.prodModelObj.get_all_products()

    def removeProduct(self,prodKey):
        return self.prodModelObj.removeProduct(prodKey)

    def isProductInSystem(self,prodKey):
        for key in self.prodModelObj.getProd().keys():
            if key == prodKey:
                return True
        return False
