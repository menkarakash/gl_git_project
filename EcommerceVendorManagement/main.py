from Ecommerce_Vendor_Management.Main_Execution.EVM import evmSys

evmSysObj = evmSys()
isExit=""

while True:
    if "X".__eq__(isExit):
        print(" Exiting from Ecommerce_Vendor_Management ")
        break
    evmSysObj.printVendorSts()
    inputVendorStr = evmSysObj.getVendorSysInput()
    if "1".__eq__(inputVendorStr):
        unStr = evmSysObj.getUserNameInput()
        evmSysObj.runCheckUserPresentInSystem(unStr)
        isExit = evmSysObj.getExitInput()
        if "X".__eq__(isExit):
            break
    elif "2".__eq__(inputVendorStr):
        unStr = evmSysObj.getUserNameInput()
        pwStr=evmSysObj.getPasswordInput()
        val= evmSysObj.runLogin(unStr,pwStr)
        if val:
            while True:
                evmSysObj.printProductSts()
                print(" ** To LogOut System (Press 4) ")
                print(" ======================================== ")
                inputProdStr= input(" Enter your input ( A/B/C/D/4 ): ")
                if "A".__eq__(inputProdStr):
                    evmSysObj.runAllProduct()
                    isExit = evmSysObj.getExitInput()
                    if "X".__eq__(isExit):
                        break
                elif "B".__eq__(inputProdStr):
                    prodkey= input(" Enter Product Name you want to remove : ")
                    if evmSysObj.runCheckProductPresentInSystem(prodkey):
                        evmSysObj.runRemoveProduct(prodkey)
                    isExit = evmSysObj.getExitInput()
                    if "X".__eq__(isExit):
                        break
                elif "C".__eq__(inputProdStr):
                    prodkey = input(" Enter Product Name you want to Add : ")
                    prodType = input(" Enter product type you want to Add : ")
                    prodAvailableQuantity = input(" Enter Product available quantity you want to Add : ")
                    prodVal = input(" Enter Product Price you want to Add : ")
                    evmSysObj.runAddProduct(prodkey, prodVal, prodType, prodAvailableQuantity)
                    isExit = evmSysObj.getExitInput()
                    if "X".__eq__(isExit):
                        break
                elif "D".__eq__(inputProdStr):
                    prodkey = input(" Enter Product Name you want to Search : ")
                    evmSysObj.runSearchProduct(prodkey)
                    isExit = evmSysObj.getExitInput()
                    if "X".__eq__(isExit):
                        break
                elif "4".__eq__(inputProdStr):
                    val = evmSysObj.runCheckUserPresentInSystem(unStr)
                    if val:
                        evmSysObj.runLogOut(unStr)
                    isExit = evmSysObj.getExitInput()
                    if "X".__eq__(isExit):
                        break
                else:
                    print(" Entered input is inValid ")
                    isExit = evmSysObj.getExitInput()
                    if "X".__eq__(isExit):
                        break
        else:
            isExit = evmSysObj.getExitInput()
            if "X".__eq__(isExit):
                break
    elif "3".__eq__(inputVendorStr):
        unStr = evmSysObj.getUserNameInput()
        pwStr = evmSysObj.getPasswordInput()
        evmSysObj.runAddUser(unStr,pwStr)
        print(" Kindly RUN Again And Login ")
        isExit = evmSysObj.getExitInput()
        if "X".__eq__(isExit):
            break
    elif "4".__eq__(inputVendorStr):
        val = evmSysObj.runCheckUserPresentInSystem(unStr)
        if val:
            evmSysObj.runLogOut(unStr)
        if "X".__eq__(isExit):
            break
    else:
        print(" Entered input is inValid ")
        isExit = evmSysObj.getExitInput()
        if "X".__eq__(isExit):
            break

