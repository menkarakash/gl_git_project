from Ecommerce_Vendor_Management.Implementation.ProductsImplementation import productImpl
from Ecommerce_Vendor_Management.Implementation.VendorImplementation import vendorImpl


class evmSys:
    def __init__(self):
        self.__productObj= productImpl()
        self.__vendorObj = vendorImpl()

    def setUserNameVarInProdObj(self,userName):
        return self.__productObj.setUser(userName)

    def printVendorSts(self):
        print(" ============================================ ")
        print(" Welcome to Ecommerce Vendor Management System ")
        print(" ============================================ ")
        print(" ** To Check User in System (Press 1) ")
        print(" ** To Login System (Press 2) ")
        print(" ** To Add your User  System (Press 3) ")
        print(" ** To LogOut System (Press 4) ")
        print(" ============================================ ")

    def getVendorSysInput(self):
        inputVendorStr = input(" Enter your input (1/2/3/4): ")
        return inputVendorStr

    def getUserNameInput(self):
        inputUser = input(" Enter UserName : ")
        return inputUser

    def getExitInput(self):
        inputExitStr = input(" Enter X to Exit / Enter anykey to RUN Again: ")
        return inputExitStr

    def getPasswordInput(self):
        inputpwd = input(" Enter Password : ")
        return inputpwd

    def getProductSysInput(self):
        inputVendorStr = input(" Enter your input (A/B/C/D): ")
        return inputVendorStr

    def runLogOut(self,userName):
        return self.__vendorObj.logout(userName)

    def runLogin(self, userName, pwd):
        return self.__vendorObj.login(userName, pwd)

    def runAddUser(self, userName, pwd):
        return self.__vendorObj.setUserInfo(userName, pwd)

    def runCheckUserPresentInSystem(self, userName):
        if self.__vendorObj.isUserPresentAndActiveInSystem(userName):
            print(" User is Active in System ")
            return True
        else:
            print(" User is not Active in System ")
            print(" Kindly Verify your User Name ")
            return False

    def printProductSts(self):
        print(" ============================================ ")
        print(" ** To get All Product (Press A) ")
        print(" ** To remove Product (Press B) ")
        print(" ** To add Product (Press C) ")
        print(" ** To search Product (Press D) ")
        print(" ============================================ ")

    def runAllProduct(self):
        return self.__productObj.all_products()

    def runRemoveProduct(self, prodKey):
        return self.__productObj.removeProduct(prodKey)

    def runAddProduct(self,prodKey, prodVal, prodType, prodAvailableQuantity):
        return self.__productObj.add_product(prodKey, prodVal, prodType, prodAvailableQuantity)

    def runSearchProduct(self, prodKey):
        return self.__productObj.search_product(prodKey)

    def runCheckProductPresentInSystem(self, prodKey):
        if self.__productObj.isProductInSystem(prodKey):
            print(" Product is in System ")
            return True
        else:
            print(" Product is not in System ")
            return False

