"""
LinkedList Class:
    This is responsible for implementing Linked List for the tasks and is used for implementing
    various aggregate operations.

"""
class LinkedList:
    #Constructor Implementation
    def __init__(self):
        self.head = None
        self.tail=None
        
    #This method will return the head of the linked list
    def get_list_head(self):
        return self.head

    # This method will return the tail of the linked list
    def get_list_Tail(self):
        return self.tail
    
    #This method is responsible for printing the linked list nodes
    def print_linked_list(self):
        temp = self.head
        while(temp is not None):
            taskObj=temp.taskObj
            taskObj.disply()
            temp=temp.next

    #This method is responsible for inserting node in the linked list
    def insert_node(self, node):
        if self.head is None:
            self.head=node
            return
        temp=self.head
        while (temp.next is not None):
            temp=temp.next
        temp.next=node
        node.prev=temp
        self.tail=node
    
    #This method is responsible for printing the linked list nodes in reverse order
    def print_in_reverse(self, node):
        temp = self.tail
        while (temp is not None):
            taskObj = temp.taskObj
            taskObj.disply()
            temp = temp.prev

    #This method is responsible for printing the linked list nodes
    def getTimeTakenByTaskList(self):
        self.timeTakenByTaskList=[]
        temp = self.head
        while(temp is not None):
            taskObj=temp.taskObj
            self.timeTakenByTaskList.append(taskObj.getTimeTakenByTask())
            temp=temp.next
        return self.timeTakenByTaskList.sort()
