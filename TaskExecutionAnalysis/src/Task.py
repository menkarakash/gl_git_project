class Task:

    def __init__(self,taskID,start_Time,end_Time):
        self._taskID=taskID
        self.__start_Time=start_Time
        self.__end_Time=end_Time
        self.__timeTakenByTask=end_Time-start_Time

    @property
    def getTaskID(self):
        return self._taskID
    def getStartTime(self):
        return self.__start_Time
    def getEndTime(self):
        return self.__end_Time
    def getTimeTakenByTask(self):
        return self.__timeTakenByTask

    def disply(self):
        print(f" [Node : Task ID - {self._taskID}, Start Time - {self.__start_Time}, End Time - {self.__end_Time},  Time Taken By Task - {self.__timeTakenByTask}]")



