class Node:
    def __init__(self,task):
        self.taskObj=task
        self.prev=None
        self.next=None

    def setPrevPointer(self,val):
        self.prev=val
    def setNextPointer(self,val):
        self.next=val

    @property
    def getTaskObj(self):
        return self.taskObj
    def getPrevPointer(self):
        return self.prev
    def getNextPointer(self):
        return self.next