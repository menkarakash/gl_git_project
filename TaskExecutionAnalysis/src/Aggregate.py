from src.LinkedList import LinkedList

"""
Aggregate class is responsible for implementating various operations on the linked list
    1. Get Minimum Timed Task Details
    2. Get Maximum Timed Task Details
    3. Get Average of all the execution times of the tasks pushed in the linked list
"""
class Aggregate:
    
    #Initializing linked list object for various operations 
    def __init__(self, linked_list:LinkedList):
        self.linked_list = linked_list
        self.timeTakenByTaskList = []
    
    #Function responsible for searching the task having maximum execution time among all the tasks
    def get_maximised_time_task(self):
        self.setTimeTakenByTaskList()
        timeTakenByTaskList = self.linked_list.getTimeTakenByTaskList()
        head = self.linked_list.get_list_head()
        temp = head
        while (temp is not None):
            taskObj = temp.taskObj
            if self.timeTakenByTaskList[-1] == taskObj.getTimeTakenByTask():
                return taskObj.getTimeTakenByTask(), taskObj._taskID
            temp = temp.next
    
    #Function responsible for searching the task having minimum execution time among all the tasks
    def get_minimised_timed_task(self):
        self.setTimeTakenByTaskList()
        timeTakenByTaskList=self.linked_list.getTimeTakenByTaskList()
        head=self.linked_list.get_list_head()
        temp = head
        while(temp is not None):
            taskObj=temp.taskObj
            if self.timeTakenByTaskList[0] == taskObj.getTimeTakenByTask():
                return taskObj.getTimeTakenByTask(), taskObj._taskID
            temp=temp.next

    #This method is responsible for printing the linked list nodes
    def setTimeTakenByTaskList(self):
        head = self.linked_list.get_list_head()
        temp = head
        while(temp is not None):
            taskObj=temp.taskObj
            self.timeTakenByTaskList.append(taskObj.getTimeTakenByTask())
            temp=temp.next
        return self.timeTakenByTaskList.sort()

    #Function responsible for calculating average of the all execution times of the tasks in the linked list
    def get_average_time_of_all_tasks(self):
        self.setTimeTakenByTaskList()
        timeTakenByTaskList = self.linked_list.getTimeTakenByTaskList()
        head = self.linked_list.get_list_head()
        temp = head
        count = 0
        total = 0
        while (temp is not None):
            taskObj = temp.taskObj
            count += 1
            total +=taskObj.getTimeTakenByTask()
            temp = temp.next
        return total/count , total, count